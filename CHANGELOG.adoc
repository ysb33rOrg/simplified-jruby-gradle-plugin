= Changelog

== 2.0.0

// tag::changelog[]

== Bugs

* {issue}4[#4] - Cleanup and document `JRubyExec`.
* {issue}5[#5] - `AbstractJRubyExec` misses `-rjars/setup`.
* {issue}8[#8] - When a script fails with an error, no output is forwarded.

== Other

* Changed the build system to use YSF Project Plugin Suite
* Upgraded to Grolifant 5.0.0

== Breaking changes

* Minimum supported Gradle is 7.3.
* Tasks no longer hold a reference to Grolifant's `ProjectOperations`, but do offer access to Grolifant`s `ConfigCacheSafeOperations`.

== Contributors

* A huge thanks to https://gitlab.com/boris-petrov[Boris Petrov] for all the testing and feedback.

// end::changelog[]

== 1.0.1 - 1.0.2

This is the initial release  with some bug fixes.

