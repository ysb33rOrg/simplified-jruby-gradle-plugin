/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.base

import org.ysb33r.gradle.jruby.testfixtures.IntegrationTestSpecification
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.FAILED
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class JRubyExecSpec extends IntegrationTestSpecification {

    public static final String TASK_NAME = 'execScript'

    @Unroll
    void 'Can execute simple script with no dependencies and #cc configuration cache'() {
        setup:
        final target = writeHelloWorld()
        writeBuildFile """
            script {
                path = file('${target.name}')
            }
        """

        when:
        final result = (ccMode ? getGradleRunnerConfigCache([TASK_NAME, '-s']) :
            getGradleRunner([TASK_NAME, '-s'])).build()

        then:
        result.task(":${TASK_NAME}").outcome == SUCCESS
        result.output.contains('Hello, World')

        where:
        cc        | ccMode
        'without' | false
        'with'    | true
    }

    void 'Output is still forwarded when an error is encountered in a script'() {
        setup:
        final target = writeHelloWorld(true)
        writeBuildFile """
            script {
                path = file('${target.name}')
            }
            process {
                ignoreExitValue = true
            }
        """

        when:
        final result = getGradleRunner([TASK_NAME, '-s']).buildAndFail()

        then:
        result.task(":${TASK_NAME}").outcome == FAILED
        result.output.contains('Hello, World')
    }

    void 'Can resolve jruby version when configuration cache is active'() {
        setup:
        final taskName = 'validateJRubyVersion'
        writeBuildFile()
        buildFile << """
        tasks.register('${taskName}') {
            final jv = tasks.${TASK_NAME}.jruby.jrubyVersionProvider

            doLast {
                println jv.get()
            }
        }
        """.stripIndent()

        when:
        final result = getGradleRunnerConfigCache([taskName, '-s']).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
    }

    private void writeBuildFile(String taskConfig = '') {
        buildFile << """
        import org.ysb33r.gradle.jruby.api.base.tasks.JRubyExec
        plugins {
            id 'org.ysb33r.jruby.base'
        }
        
        repositories {
            mavenCentral()
        }

        tasks.register( '${TASK_NAME}', JRubyExec ) {
            ${taskConfig}
        }
        """.stripIndent()
    }

    private File writeHelloWorld(boolean addError = false) {
        final target = new File(projectDir, 'helloWorld.rb')
        target.text = 'puts "Hello, World"'
        if (addError) {
            target << '''
            exit 1
            '''.stripIndent()
        }
        target
    }
}