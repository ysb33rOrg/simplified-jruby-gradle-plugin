// tag::simple-gem-resolver[]
import org.ysb33r.gradle.jruby.kt.resolver.ruby
// end::simple-gem-resolver[]

plugins {
    id("org.ysb33r.jruby.resolver.kt")
}

// tag::simple-gem-resolver[]
val something by configurations.creating

dependencies {
    something("rubygems:credit_card_validator:1.3.2")
}

repositories {
    ruby.gems()
}
// end::simple-gem-resolver[]

tasks.register("runGradleTest") {
    doLast { configurations.getByName("something").getFiles() }
}
