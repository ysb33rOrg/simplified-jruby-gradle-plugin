/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.kt.resolver

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.jruby.testfixtures.IntegrationTestSpecification

class RepositoryHandlerIntegrationSpec extends IntegrationTestSpecification {
    void 'Startup a server inside a Gradle project'() {
        when:
        buildFile.text = '''
        import org.ysb33r.gradle.jruby.kt.resolver.ruby
        
        plugins {
            id("org.ysb33r.jruby.resolver.kt")
        }
        
        val something by configurations.creating 
        
        dependencies {
            something("rubygems:credit_card_validator:1.3.2")
        }
        
        repositories {
            ruby.gems()
        }
        '''

        BuildResult result = GradleRunner.create()
            .withProjectDir(projectDir)
            .withPluginClasspath()
            .withTestKitDir(testKitDir)
            .withArguments(['dependencies', '--configuration=something', '-i', '-s'])
            .forwardOutput()
            .withDebug(true)
            .build()

        then:
        result.output.contains('rubygems:credit_card_validator:1.3.2')
        result.output.contains('rubygems:base_app:[1.0.5,) ->')
    }

}