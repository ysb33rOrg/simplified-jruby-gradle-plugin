/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.core

import groovy.transform.CompileStatic
import org.ysb33r.gradle.jruby.api.gems.GemUtils
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.executable.ScriptDefinition
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmScriptExecSpec

/**
 * Specifies a JRuby execution.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class JRubyExecSpec extends AbstractJvmScriptExecSpec<JRubyExecSpec> {

    public static final String LOAD_JARS_LOCK_ON_STARTUP = '-rjars/setup'

    /**
     * Create new JRuby execution specification.
     *
     * @param ccso Configuration cache-safe operations
     *
     * @since 2.0
     */
    JRubyExecSpec(ConfigCacheSafeOperations ccso) {
        super(ccso, new JRubyScriptDefinition(ccso))

        entrypoint {
            mainClass = GemUtils.JRUBY_MAINCLASS
        }

        runnerSpec {
            args(LOAD_JARS_LOCK_ON_STARTUP)
        }
    }

    @Deprecated
    JRubyExecSpec(ProjectOperations projectOperations) {
        super(
            ConfigCacheSafeOperations.from(projectOperations),
            new JRubyScriptDefinition(ConfigCacheSafeOperations.from(projectOperations))
        )

        entrypoint {
            mainClass = GemUtils.JRUBY_MAINCLASS
        }

        runnerSpec {
            args(LOAD_JARS_LOCK_ON_STARTUP)
        }
    }

    private static class JRubyScriptDefinition extends ScriptDefinition {
        JRubyScriptDefinition(ConfigCacheSafeOperations po) {
            super(po)
            super.setDefaultPreArgsProvider(po.providerTools().provider { ->
                (name.present ? ['-S', name.get()] : [po.stringTools().stringize(path.get())])
            })
        }
    }
}
