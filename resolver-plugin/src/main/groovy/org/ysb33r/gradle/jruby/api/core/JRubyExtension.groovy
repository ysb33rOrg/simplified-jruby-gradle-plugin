/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.core

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.jruby.internal.core.PluginMetadata
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.CombinedProjectTaskExtensionBase

import java.util.function.Function

import static org.ysb33r.gradle.jruby.internal.exec.JRubyExecUtils.JRUBY_COMPLETE
import static org.ysb33r.gradle.jruby.internal.gems.GemPrepareUtils.createJRubyConfigurationName

/**
 * Class providing the JRuby DSL extension which can be added to a task or a project.
 *
 * @author Schalk W. Cronjé
 * @author R Tyler Croy
 * @author Christian Meier
 *
 */
@CompileStatic
class JRubyExtension extends CombinedProjectTaskExtensionBase<JRubyExtension> {
    public static final String NAME = 'jruby'
    public static final String JRUBY_COMPLETE_DEPENDENCY = "org.jruby:${JRUBY_COMPLETE}"

    /**
     * Project extension constructor.
     *
     * @param p Project to attach extension to.
     */
    JRubyExtension(Project p) {
        this(p, NAME)
    }

    /**
     * Project extension constructor.
     *
     * @param p Project to attach extension to.
     * @param name Used when creating a custom JRubyExtension in a plugin.
     */
    JRubyExtension(Project p, String name) {
        super(ProjectOperations.find(p))
        this.jrubyVersionProperty = p.objects.property(String)
        this.jrubyVersionProperty.set(PluginMetadata.jrubyVersion())
        this.jrubyVersionResolver = this.jrubyVersionProperty
        this.jrubyConfiguration = p.configurations.create(createJRubyConfigurationName(name, null))

        this.jrubyDependencyCreator = { DependencyHandler dependencies, String ver ->
            dependencies.create(ver)
        }.curry(p.dependencies) as Function<String, Dependency>
        initJRubyDependencies(p)
    }

    /**
     * Task extension constructor.
     *
     * @param t Task to attach extension to.
     * @param projectExtension Project-attached {@code JRubyExtension}. Can be null.
     */
    JRubyExtension(Task t, JRubyExtension projectExtension) {
        this(t, projectExtension, NAME)
    }

    /**
     * Task extension constructor.
     * <p>
     * This variation is used by plugin authors to attach custom JRubyExtension instances to their own tasks or
     * extensions when their is nor global project extension by design.
     *
     * @param t Task to attach extension to.
     * @param name Used when creating a custom JRubyExtension in a plugin.
     */
    JRubyExtension(Task t, String name) {
        this(t, null, name)
    }

    /**
     * Task extension constructor.
     * <p>
     * This variation is used by plugin authors to attach custom JRubyExtension instances to their own tasks or
     * extensions.
     *
     * @param t Task to attach extension to.
     * @param projectExtension Project-attached {@code JRubyExtension}. Can be null.
     * @param name Used when creating a custom JRubyExtension in a plugin.
     */
    JRubyExtension(Task t, JRubyExtension projectExtension, String name) {
        super(t, ProjectOperations.find(t.project), projectExtension)
        this.jrubyVersionProperty = t.project.objects.property(String)

        if (projectExtension) {
            this.jrubyVersionResolver = this.jrubyVersionProperty.orElse(projectExtension.jrubyVersionProvider)
        } else {
            this.jrubyVersionResolver = this.jrubyVersionProperty
            this.jrubyVersionProperty.set(PluginMetadata.jrubyVersion())
        }

        // TODO: Fix to be resolvable OR something
        this.jrubyConfiguration = t.project.configurations.maybeCreate(createJRubyConfigurationName(name, t.name))
        initJRubyDependencies(t.project)
    }

    /**
     * The default version of jruby that will be used.
     *
     * @return Returns the JRubyVersion to use.
     */
    String getJrubyVersion() {
        this.jrubyVersionResolver.get()
    }

    /**
     * The default version of jruby that will be used.
     *
     * @return Returns the JRubyVersion to use.
     *
     * @since 2.0
     */
    Provider<String> getJrubyVersionProvider() {
        this.jrubyVersionResolver
    }

    /**
     * Set a new JRuby version to use.
     *
     * @param v New version to be used. Can be of anything that be resolved by
     * {@link org.ysb33r.grolifant5.api.core.StringTools#stringize}
     */
    void setJrubyVersion(Object v) {
        projectOperations.stringTools.updateStringProperty(this.jrubyVersionProperty, v)
    }

    /**
     * Returns a runConfiguration of the configured JRuby core dependencies.
     *
     * @return A provider to detached runConfiguration.
     */
    Configuration getJrubyConfiguration() {
        this.jrubyConfiguration
    }

    private void initJRubyDependencies(Project project) {
        jrubyConfiguration.defaultDependencies { ds ->
            final String jrubyVer = jrubyVersion
            final String jrubyCompleteDep = "${JRUBY_COMPLETE_DEPENDENCY}:${jrubyVer}"
            ds.add(project.dependencies.create(jrubyCompleteDep))
        }
    }

    private final Property<String> jrubyVersionProperty
    private final Provider<String> jrubyVersionResolver
    private final Function<String, Dependency> jrubyDependencyCreator
    private final Configuration jrubyConfiguration
}
