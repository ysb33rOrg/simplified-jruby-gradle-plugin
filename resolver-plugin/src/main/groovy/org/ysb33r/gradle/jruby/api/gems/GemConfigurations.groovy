/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.gems

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.gradle.jruby.api.tasks.AbstractJRubyExecTask
import org.ysb33r.gradle.jruby.internal.gems.GemPrepareUtils
import org.ysb33r.grolifant5.api.core.ProjectOperations

import java.util.function.Function

/**
 * Creates or registers configurations that will contain GEMs.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class GemConfigurations {
    public static final String NAME = 'gemConfigurations'
    public static final String DEFAULT_TASK_GROUP = 'GEM preparation'

    GemConfigurations(Project project) {
        this.projectOperations = ProjectOperations.find(project)
        this.taskContainer = project.tasks

        final configTools = this.projectOperations.configurations
        this.configurationCreator = { ConfigurationContainer configurations, String name ->
            configTools.createLocalRoleFocusedConfiguration(name, "${name}GemClasspath")
            [
                configurations.getByName(name),
                configurations.getByName("${name}GemClasspath")
            ]
        }.curry(project.configurations) as Function<String, List<Configuration>>
    }

    /**
     * Registers a configuration that will contain GEMs.
     *
     * If the configuration does not exist it will be created.
     *
     * An associated task to prepare the GEMs will also be registered.
     *
     * @param c Character sequence as configuration name
     * @param prepareClass Task type to create.
     * @return Provider to a configuration
     */
    public <T extends GemPrepareTask> RegisteredGemConfiguration<T> register(
        final CharSequence c,
        Class<T> prepareClass
    ) {
        String name = c
        final configurations = configurationCreator.apply(name)
        registeredAs(configurations[0], registerPrepareTask(prepareClass, name, configurations[1]))
    }

    /**
     * Registers a configuration, a prepare task and an execution task.
     *
     * <p>
     * If the configuration does not exist it will be created.
     * An associated task to prepare the GEMs will also be registered.
     * A execution task will be registered and will depend on the registered prepare task.
     * </p>
     *
     * @param c Character sequence as configuration name
     * @param prepareClass Task type to create.
     * @param execClass Task type to create.
     *   The created task's name will be prefixed with {@code run} and followed by the
     *   capitalized version of {@code name}.
     * @return Provider to a configuration
     *
     * @since 2.0
     */
    public <T extends GemPrepareTask, E extends AbstractJRubyExecTask> RegisteredGemConfiguration<T> register(
        final CharSequence c,
        Class<T> prepareClass,
        Class<E> execClass
    ) {
        String name = c
        final configurations = configurationCreator.apply(name)
        final prepare = registerPrepareTask(prepareClass, name, configurations[1])
        registerExecTask(execClass, prepare, "run${name.capitalize()}")
        registeredAs(configurations[0], prepare)
    }

    /**
     * Registers a configuration, a prepare task and an execution task.
     *
     * <p>
     * If the configuration does not exist it will be created.
     * An associated task to prepare the GEMs will also be registered.
     * A execution task will be registered and will depend on the registered prepare task.
     * </p>
     *
     * @param c Character sequence as configuration name
     * @param prepareClass Task type to create.
     * @param execClass Task type to create.
     * @param execTaskName Name of exec task
     * @return Provider to a configuration
     *
     * @since 2.0
     */
    public <T extends GemPrepareTask, E extends AbstractJRubyExecTask> RegisteredGemConfiguration<T> register(
        final CharSequence c,
        Class<T> prepareClass,
        Class<E> execClass,
        String execTaskName
    ) {
        String name = c
        final configurations = configurationCreator.apply(name)
        final prepare = registerPrepareTask(prepareClass, name, configurations[1])
        registerExecTask(execClass, prepare, execTaskName)
        registeredAs(configurations[0], prepare)
    }

    /**
     * Registers a configuration that will contain GEMs.
     *
     * An associated task to prepare the GEMs will also be registered.
     *
     * @param resolvable Configuration instance which can be resolved.
     * @param prepareClass Task type to create.
     */
    public <T extends GemPrepareTask> RegisteredGemConfiguration<T> register(
        final Configuration resolvable,
        Class<T> prepareClass
    ) {
        registeredAs(resolvable, registerPrepareTask(prepareClass, resolvable.name, resolvable))
    }

    private <T extends GemPrepareTask> TaskProvider<T> registerPrepareTask(
        Class<T> taskType,
        String baseName,
        Configuration resolver
    ) {
        final String taskName = GemPrepareUtils.taskName(baseName)
        final String gemDir = GemPrepareUtils.gemRelativePath(baseName)
        TaskProvider<T> prepare = taskContainer.register(taskName, taskType)
        prepare.configure(new Action<T>() {
            void execute(T jp) {
                jp.group = DEFAULT_TASK_GROUP
                jp.description = "Prepare the gems/jars from the `${baseName}` dependencies"
                jp.outputDir = projectOperations.buildDirDescendant(gemDir)
                jp.gemConfiguration = resolver
            }
        })
        prepare
    }

    private <E extends AbstractJRubyExecTask> TaskProvider<E> registerExecTask(
        Class<E> taskType,
        TaskProvider<? extends GemPrepareTask> prepare,
        String taskName
    ) {
        TaskProvider<E> exec = taskContainer.register(taskName, taskType)
        exec.configure(new Action<E>() {
            void execute(E jp) {
                jp.group = DEFAULT_TASK_GROUP
                jp.gemDirProvider = prepare.flatMap { ((GemPrepareTask) it).outputDir }
                jp.dependsOn(prepare)
            }
        })
    }

    private <T extends GemPrepareTask> RegisteredGemConfiguration<T> registeredAs(
        Configuration cfg,
        TaskProvider<T> task
    ) {
        new RegisteredGemConfiguration<T>() {
            final Configuration configuration = cfg
            final TaskProvider<T> prepareTask = task
        }
    }

    private final Function<String, List<Configuration>> configurationCreator
    private final ProjectOperations projectOperations
    private final TaskContainer taskContainer
}
