/**
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.gems;

import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.file.FileCollection;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.PathSensitive;
import org.ysb33r.grolifant5.api.core.runnable.JvmExecutionModel;

import java.io.File;
import java.util.Locale;

import static org.gradle.api.tasks.PathSensitivity.NAME_ONLY;
import static org.ysb33r.gradle.jruby.api.gems.GemUtils.getGems;

/**
 * Specification of a task that can prepare GEMs for usage by JRuby.
 *
 * @author Schalk W. Cronjé.
 */
public interface GemPrepareTask extends Task, JvmExecutionModel {

    void setGemOverwriteAction(GemOverwriteAction action);

    default void setGemOverwriteAction(String action) {
        setGemOverwriteAction(GemOverwriteAction.valueOf(action.toUpperCase(Locale.US)));
    }

    /**
     * Target directory for GEMs. Extracted GEMs will end up in {@code outputDir + "/gems"}.
     *
     * @return Provider to a destination directory
     */
    @OutputDirectory
    Provider<File> getOutputDir();

    /**
     * Sets the output directory.
     *
     * @param f Output directory
     */
    void setOutputDir(Object f);

    /**
     * All GEMs that have been supplied as dependencies.
     *
     * @return Collection of GEMs.
     */
    @Internal
    default FileCollection getGemsAsFileCollection() {
        return getGems(getGemsAndJarsAsFileCollection());
    }

    @InputFiles
    @PathSensitive(NAME_ONLY)
    FileCollection getGemsAndJarsAsFileCollection();

    /**
     * Sets the GEM configuration to be used with this task.
     *
     * @param fc Provides a configuration or other collection of files that will contain GEM dependencies.
     */
    void setGemConfiguration(Configuration fc);
}
