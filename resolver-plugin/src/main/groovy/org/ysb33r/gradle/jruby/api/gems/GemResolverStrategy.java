/**
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.gems;

import groovy.lang.Closure;
import org.codehaus.groovy.runtime.DefaultGroovyMethods;
import org.codehaus.groovy.runtime.StringGroovyMethods;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ModuleVersionSelector;
import org.ysb33r.gradle.jruby.internal.gems.DefaultGemResolverStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Defines groups which contains GEMs and controls GEM resolving rules.
 *
 * @author Schalk W. Cronjé
 */
public interface GemResolverStrategy {

    /**
     * Is this group/organisation a GEM group ?
     *
     * @param groupName Name of group/organisation.
     * @return {@code true} is group is a GEM group.
     */
    boolean isGemGroup(final String groupName);

    /**
     * Add a new group for GEMs.
     *
     * @param groupName Name of group to add.
     */
    void addGemGroup(final String groupName);

    /**
     * Exclude a configuration from being resolved using the GEM
     * version resolver strategy.
     *
     * @param configs Configurations to be excluded
     */
    void excludeConfigurations(Configuration... configs);

    /**
     * Exclude a configuration from being resolved using the GEM
     * version resolver strategy.
     *
     * @param configs Configurations to be excluded
     */
    void excludeConfigurations(String... configs);

    /**
     * Exclude a module from being resolved using the GEM version resolver
     * strategy.
     *
     * @param name    Module name. Never {@code null}.
     * @param version Version. Can be {@code null}.
     */
    void excludeModule(String name, String version);

    /**
     * Exclude a module from being resolved using the GEM version resolver
     * strategy.
     *
     * @param name    Module name. Never {@code null}.
     */
    void excludeModule(String name);

    /**
     * Exclude a module from being resolved using the GEM version resolver
     * strategy.
     *
     * @param namePattern    Pattern for name. Never {@code null}.
     * @param versionPattern Pattern for version. Can be {@code null}
     */
    void excludeModule(Pattern namePattern, Pattern versionPattern);

    /**
     * Exclude a module from being resolved using the GEM version resolver
     * strategy.
     *
     * @param namePattern    Pattern for name. Never {@code null}.
     */
    void excludeModule(Pattern namePattern);

    /**
     * Whether the GEM version resolving strategy should be applied for a specific module.
     * <p>
     * In most cases this will always be {@code true} unless a specific rule excludes it.
     *
     * @param mvs Module version selector
     * @return Whether the Bundler-like version selector atregty may be applied
     */
    boolean useGemVersionResolver(final ModuleVersionSelector mvs);

    /**
     * Whether the GEM version resolving strategy should be applied to a specific configuration.
     * <p>
     * In most cases this will always be {@code true} unless a specific rule excludes it.
     *
     * @param configurationName Name fo configuration
     * @return Whether the Bundler-like version selector strategy may be applied
     */
    boolean useGemVersionResolver(String configurationName);
}
