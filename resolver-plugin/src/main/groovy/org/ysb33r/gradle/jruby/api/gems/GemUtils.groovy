/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.gems

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Action
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DuplicateFileCopyingException
import org.gradle.api.file.FileCollection
import org.ysb33r.grolifant5.api.core.ProjectOperations

import static GemOverwriteAction.FAIL
import static GemOverwriteAction.OVERWRITE

/**
 * A collection of utilities to manipulate GEMs.
 *
 * @author R Tyler Croy
 * @author Schalk W. Cronjé
 */
@CompileStatic
@Slf4j
class GemUtils {
    public static final String GEM_COMMAND = 'gem'
    public static final String JRUBY_MAINCLASS = 'org.jruby.Main'
    public static final String JRUBY_COMPLETE_NAME = 'jruby-complete'
    public static final String JARS_LOCK = 'Jars.lock'
    public static final String JARS_REWRITE_SUBDIR = 'jars'

    /**
     * Contains which GEMs to unpack and which old GEMs to delete.
     *
     */
    static class GemsToProcess {
        GemsToProcess(List<File> unpack, Set<File> delete) {
            this.unpack = unpack
            this.delete = delete
        }
        final List<File> unpack
        final Set<File> delete
    }

    /**
     * Information on which JAR-dependent GEMs need to be processed from a configuration.
     */
    static class JarsToProcess {
        final Set<File> jars = []
        final List<String> coordinates = []
        final Map<String, String> fileRenameMap = [:]

        JarsToProcess() {
        }

        JarsToProcess(
            Iterable<File> jars,
            Map<String, String> fileRenameMap
        ) {
            this.jars.addAll(jars)
            this.fileRenameMap.putAll(fileRenameMap)
        }
    }

    /**
     * Given a FileCollection return a filtered FileCollection only containing GEMs
     *
     * @param fc Original FileCollection
     * @return Filtered FileCollection
     */
    static FileCollection getGems(FileCollection fc) {
        fc.filter { File f ->
            f.name.toLowerCase(Locale.US).endsWith(GEM_EXTENSION)
        }
    }

    /**
     * Finds a JRuby Jar within a file collection
     * @param fc File collection to search
     * @param version Version of {@link #JRUBY_COMPLETE_NAME} to search for.
     * @return Location of JAR.
     * @throw java.lang.IllegalStateException if there is more than one match or no match.
     */
    static File findJRubyJar(FileCollection fc, String version) {
        fc.filter { File f ->
            f.name.toLowerCase(Locale.US) =~ ~/${JRUBY_COMPLETE_NAME}-${version}\.jar/
        }.singleFile
    }

    /**
     * Process a collection of GEMs and determine which to delete and which to unpack on the local filesystem.
     *
     * @param gems GEM collection.
     * @param destDir GEM destination directory.
     * @param overwrite GEM overwrite mode.
     * @return Collections of files to unpack and delete.
     */
    static GemsToProcess findGemsToProcess(
        FileCollection gems,
        File destDir,
        GemOverwriteAction overwrite
    ) {
        List<File> gemsToProcess = []
        Set<File> deletes = []

        getGems(gems).files.each { File gem ->
            String gemName = gemFullNameFromFile(gem.name)
            File extractDir = new File(destDir, "gems/${gemName}")
            // We want to check for -java specific gem installations too, e.g.
            // thread_safe-0.3.4-java
            File extractDirForJava = new File(destDir, "gems/${gemName}-java")

            switch (overwrite) {
                case GemOverwriteAction.SKIP:
                    if (extractDir.exists() || extractDirForJava.exists()) {
                        return
                    }
                case OVERWRITE:
                    deletes.add(extractDir)
                    deletes.add(extractDirForJava)
                    break
                case FAIL:
                    if (extractDir.exists() || extractDirForJava.exists()) {
                        throw new DuplicateFileCopyingException("Gem ${gem.name} already exists")
                    }
            }

            gemsToProcess.add(gem)
        }

        /* NOTE: gemsToProcess is assumed to typically be sourced from
         * a FileCollection generated elsewhere in the code. The
         * FileCollection a flattened version of the dependency tree.
         *
         * In order to handle Rubygems which depend on their
         * dependencies at _installation time_, we need to reverse the
         * order to make sure that the .gem files for the
         * transitive/nested dependencies are installed first
         */
        new GemsToProcess(gemsToProcess.reverse(), deletes)
    }

    /**
     * Extract JARs from a GEM configuration.
     *
     * @param config GEM configuration.
     *
     * @return A description of which JARs to use and rename
     */
    @SuppressWarnings('LineLength')
    static JarsToProcess findGemDependentJars(Configuration config) {
        final jarsToProcess = new JarsToProcess()

        Set<ResolvedArtifact> artifacts = config.resolvedConfiguration.resolvedArtifacts
        artifacts.each { ResolvedArtifact dependency ->
            final group = dependency.moduleVersion.id.group
            final groupAsPath = group.replaceAll(~/\\./, File.separatorChar.toString())
            final version = dependency.moduleVersion.id.version
            final classifier = dependency.classifier ? "-${dependency.classifier}".toString() : ''
            final newFileName = "${groupAsPath}/${dependency.name}/${version}/${dependency.name}-${version}${classifier}.${dependency.type}"

            if (dependency.type != GEM_DEPENDENCY_TYPE && dependency.name != JRUBY_COMPLETE_NAME) {
                // TODO classifier and system-scope
                jarsToProcess.coordinates.add("${group}:${dependency.name}:${version}:runtime:".toString())
            }
            jarsToProcess.fileRenameMap[dependency.file.name] = newFileName.toString()
            // TODO omit system-scoped files
            jarsToProcess.jars << dependency.file
        }
        jarsToProcess
    }

    /**
     * Write a JARs lock file if the content has changed.
     *
     * @param jarsLock Lock file to write to.
     * @param coordinates Coordinates.
     */
    static void writeJarsLock(File jarsLock, List<String> coordinates) {
        String content

        if (jarsLock.exists()) {
            content = jarsLock.text
        } else {
            jarsLock.parentFile.mkdirs()
            content = ''
        }
        new StringWriter().withCloseable { newContent ->
            coordinates.each { newContent.println it }
            if (content != newContent.toString()) {
                jarsLock.text = newContent
            }
        }
    }

    /** Rewrite the JAR dependencies
     *
     * @param jarsDir Target directory for JARs
     * @param jarsToProcess JARs to process
     * @param overwrite Overwrite action
     */
    static void rewriteJarDependencies(
        File jarsDir,
        JarsToProcess jarsToProcess,
        GemOverwriteAction overwrite
    ) {
        final dependencies = jarsToProcess.jars
        final renameMap = jarsToProcess.fileRenameMap
        dependencies.each { File dependency ->
            final depName = dependency.name.toLowerCase(Locale.US)
            if (depName.endsWith('.jar') && !depName.startsWith(JRUBY_COMPLETE_NAME)) {
                File destination = new File(jarsDir, renameMap[dependency.name])
                switch (overwrite) {
                    case FAIL:
                        if (destination.exists()) {
                            throw new DuplicateFileCopyingException("Jar ${destination.name} already exists")
                        }
                    case GemOverwriteAction.SKIP:
                        if (destination.exists()) {
                            break
                        }
                    case OVERWRITE:
                        destination.delete()
                        destination.parentFile.mkdirs()
                        dependency.withInputStream { destination << it }
                }
            }
        }
    }

    /**
     * Take the given .gem filename (e.g. rake-10.3.2.gem) and just return the
     * gem "full name" (e.g. rake-10.3.2)
     *
     * @param filename GEM filename.
     * @return GEM name + version.
     */
    static String gemFullNameFromFile(String filename) {
        filename.replaceAll(~GEM_EXTENSION, '')
    }

    /**
     * Adds a GEM CopySpec to an archive
     *
     * The following are supported as properties:
     * <ul>
     * <li>fullGem (boolean) - Copy all of the GEM content, not just a minimal subset</li>
     * <li>subfolder (Object) - Adds an additional subfolder into the GEM
     * </ul>
     *
     * @param Additional properties to control behaviour
     * @param dir The source of the GEM files
     * @return Returns a CopySpec which can be attached as a child to another object that implements a CopySpec
     */
    static CopySpec gemCopySpec(Map properties = [:], ProjectOperations projectOperations, Object dir) {
        boolean fullGem = properties['fullGem']
        String subFolder = properties['subfolder']

        projectOperations.copySpec(new Action<CopySpec>() {
            void execute(CopySpec spec) {
                spec.tap {
                    from(dir) { CopySpec cs ->
                        cs.tap {
                            include((String) EVERYTHING)
                            if (!fullGem) {
                                exclude 'cache/**'
                                exclude 'gems/*/test/**'
                                exclude 'gems/*/tests/**'
                                exclude 'gems/*/spec/**'
                                exclude 'gems/*/specs/**'
                                exclude 'build_info'
                            }
                        }
                    }
                    if (subFolder) {
                        into subFolder
                    }
                }
            }
        })
    }

    static CopySpec jarCopySpec(ProjectOperations projectOperations, Object dir) {
        projectOperations.copySpec { CopySpec spec ->
            spec.with {
                from(dir) { include EVERYTHING }
            }
        }
    }

    private static final String GEM_DEPENDENCY_TYPE = GEM_COMMAND
    private static final String GEM_EXTENSION = ".${GEM_COMMAND}"
    private static final String EVERYTHING = '**'
}
