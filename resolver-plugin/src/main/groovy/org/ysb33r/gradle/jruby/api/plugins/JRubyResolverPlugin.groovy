/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.provider.Provider
import org.gradle.api.services.BuildServiceSpec
import org.ysb33r.gradle.jruby.api.gems.GemConfigurations
import org.ysb33r.gradle.jruby.api.gems.GemResolverStrategy
import org.ysb33r.gradle.jruby.api.resolver.RepositoryHandlerExtension
import org.ysb33r.gradle.jruby.internal.core.PluginMetadata
import org.ysb33r.gradle.jruby.internal.gems.DefaultGemResolverStrategy
import org.ysb33r.gradle.jruby.internal.gems.GemVersionResolver
import org.ysb33r.gradle.jruby.internal.resolver.DefaultRepositoryHandlerExtension
import org.ysb33r.gradle.jruby.internal.resolver.IvyXmlProxyBuildService
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin

/**
 * Provides only a repository handler extension for looking up rubygem
 * metadata.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class JRubyResolverPlugin implements Plugin<Project> {
    public static final String PROXY_SERVICE_NAME = 'rubygemsproxy'

    @Override
    void apply(Project project) {
        project.pluginManager.apply(GrolifantServicePlugin)
        addGemResolverStrategy(project)
        addRepositoryExtension(project, createIvyXmlService(project))
        addGemConfigurationsExtension(project)
    }

    private void addGemResolverStrategy(Project project) {
        GemResolverStrategy gemGroups = project.extensions.create(
            GemResolverStrategy,
            DefaultGemResolverStrategy.NAME,
            DefaultGemResolverStrategy
        )

        project.configurations.all { Configuration cfg ->
            GemVersionResolver.addGemResolver(cfg, gemGroups, new GemVersionResolver(gemGroups, cfg))
        }
    }

    private void addGemConfigurationsExtension(Project project) {
        project.extensions.create(
            GemConfigurations.NAME,
            GemConfigurations,
            project
        )
    }

    private void addRepositoryExtension(Project project, Provider<IvyXmlProxyBuildService> serviceProvider) {
        ((ExtensionAware) project.repositories).extensions.create(
            RepositoryHandlerExtension,
            DefaultRepositoryHandlerExtension.NAME,
            DefaultRepositoryHandlerExtension,
            project,
            serviceProvider
        )
    }

    private Provider<IvyXmlProxyBuildService> createIvyXmlService(Project project) {
        boolean refresh = project.gradle.startParameter.refreshDependencies
        def cache = new File(
            project.gradle.gradleUserHomeDir,
            "rubygems-ivyxml-cache2/${PluginMetadata.version()}"
        )
        def cfg = new Action<BuildServiceSpec<IvyXmlProxyBuildService.Params>>() {
            @Override
            void execute(BuildServiceSpec<IvyXmlProxyBuildService.Params> spec) {
                spec.parameters.tap {
                    refreshDependencies.set(refresh)
                    rootCacheDir.set(cache)
                }
            }
        }

        project.gradle.sharedServices.registerIfAbsent(
            PROXY_SERVICE_NAME,
            IvyXmlProxyBuildService,
            cfg
        )
    }
}
