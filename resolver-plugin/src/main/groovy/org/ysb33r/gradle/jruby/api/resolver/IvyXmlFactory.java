/**
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.resolver;

import org.ysb33r.gradle.jruby.api.gems.GemNotFoundException;

import java.nio.file.Path;

/**
 * Builds various IvyXml-related objects.
 *
 * @author Schalk W. Cronjé
 */
public interface IvyXmlFactory {
    /**
     * Returns the path of disk wheer the ivy.xml file related to the specific Rubygem will be found.
     *
     * @param grp Maven-type group
     * @param name Rubygem name
     * @param version Rubygem version
     * @return Path on local file system
     * @throws GemNotFoundException - When there is an issue processing th GEM request
     */
    Path ivyXmlPath(String grp, String name, String version) throws GemNotFoundException;

    /**
     * Returns the path to the ivy.xml.sha1 file.
     *
     * @param grp Maven-type group
     * @param name Rubygem name
     * @param version Rubygem version
     * @return Path on local filesystem.
     * @throws GemNotFoundException - When there is an issue processing th GEM request
     */
    Path ivyXmlSha1(String grp, String name, String version) throws GemNotFoundException;

    /**
     * Lists all versions as directories.
     *
     * @param grp  Maven-type group
     * @param name Rubygem name
     * @return A directory listing of all versions
     * @throws GemNotFoundException - When there is an issue processing th GEM request
     */
    String directoryListing(String grp, String name) throws GemNotFoundException;
}
