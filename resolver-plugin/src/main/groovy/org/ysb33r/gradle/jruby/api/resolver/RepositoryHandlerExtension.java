/**
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.resolver;

import org.gradle.api.Action;
import org.gradle.api.artifacts.repositories.ArtifactRepository;
import org.gradle.api.artifacts.repositories.MavenArtifactRepository;
import org.ysb33r.gradle.jruby.internal.gems.GemRepositoryConfiguration;

/**
 * Extension which can be added to {@code project.repositories}.
 *
 * @author Schalk W. Cronjé
 */
public interface RepositoryHandlerExtension {

    /**
     * Create an artifact repository which will use https://rubygems.org and
     * associate group {@code rubygems} with it.
     *
     * @return Artifact repository.
     */
    ArtifactRepository gems();

    /**
     * Create an artifact repository which will use https://rubygems.org and
     * associate group {@code rubygems} with it.
     *
     * @param cfg GEM repository configuration
     * @return Artifact repository.
     */
    ArtifactRepository gems(Action<GemRepositoryConfiguration> cfg);

    /**
     * Create an artifact repository which will use specified URI and
     * associate group {@code rubygems} with it.
     *
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     *            with {@code project.uri} can be provided.
     * @return Artifact repository.
     */
    ArtifactRepository gems(Object uri);

    /**
     * Create an artifact repository which will use specified URI and
     * associate group {@code rubygems} with it.
     *
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     *            with {@code project.uri} can be provided.
     * @param cfg GEM repository configuration
     * @return Artifact repository.
     */
    ArtifactRepository gems(Object uri, Action<GemRepositoryConfiguration> cfg);

    /**
     * Create an artifact repository which will use specified URI and
     * associate a specified group with it.
     *
     * @param group Group to associate this server with.
     * @param uri   URI of remote repository that serves up Rubygems. Any object convertible
     *              with {@code project.uri} can be provided.
     * @return Artifact repository.
     */
    ArtifactRepository gems(String group, Object uri);

    /** Create an artifact repository which will use specified URI and
     * associate a specified group with it.
     *
     * @param group Group to associate this server with.
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     * with {@code project.uri} can be provided.
     * @param cfg GEM repository configuration
     * @return Artifact repository.
     */
    ArtifactRepository gems(String group, Object uri, Action<GemRepositoryConfiguration> cfg);

    /** Adds the Maven-GEMs proxy that is supported by the JRuby group.
     *
     * For supporting Gradle versions, this repository will only be consulted for artifacts that are in the
     * {@code rubygems} group.
     *
     * @return Maven repository
     */
    MavenArtifactRepository mavengems();

    /** Adds a remote Maven-GEMs proxy.
     *
     * For supporting Gradle versions, this repository will only be consulted for artifacts that are in the
     * {@code rubygems} group.
     *
     * @param uri Remote Maven-GEMs proxy
     * @return Maven repository
     */
    MavenArtifactRepository mavengems(Object uri);

    /**  Adds a remote Maven-GEMs proxy and allocate a dedicated group for it.
     *
     * For supporting Gradle versions, this repository will only be consulted for artifacts that are in the
     * specified group.
     *
     * @param group Maven group name
     * @param uri Remote Maven-GEMs proxy
     * @return Maven repository
     */
    MavenArtifactRepository mavengems(String group, Object uri);
}
