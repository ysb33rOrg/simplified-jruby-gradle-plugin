/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.tasks

import groovy.transform.CompileStatic
import org.gradle.api.artifacts.Configuration
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.gradle.workers.WorkerExecutor
import org.ysb33r.gradle.jruby.api.core.JRubyExecSpec
import org.ysb33r.gradle.jruby.api.errors.MissingJRubyException
import org.ysb33r.gradle.jruby.api.errors.UnsupportedConfigurationException
import org.ysb33r.gradle.jruby.api.gems.GemOverwriteAction
import org.ysb33r.gradle.jruby.api.gems.GemPrepareTask
import org.ysb33r.gradle.jruby.api.gems.GemUtils
import org.ysb33r.gradle.jruby.internal.exec.JRubyWorkerExecutor
import org.ysb33r.gradle.jruby.internal.gems.GemPrepareTaskExtension
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.jvm.ExecutionMode
import org.ysb33r.grolifant5.api.core.jvm.worker.StandardWorkerAppParameterFactory
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerPromise
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask
import org.ysb33r.grolifant5.api.remote.worker.StandardWorkerAppExecutorFactory

import static org.ysb33r.gradle.jruby.api.gems.GemUtils.GEM_COMMAND
import static org.ysb33r.gradle.jruby.api.gems.GemUtils.JARS_LOCK
import static org.ysb33r.gradle.jruby.api.gems.GemUtils.rewriteJarDependencies
import static org.ysb33r.gradle.jruby.api.gems.GemUtils.writeJarsLock
import static org.ysb33r.grolifant5.api.core.jvm.ExecutionMode.JAVA_EXEC
import static org.ysb33r.grolifant5.api.core.jvm.ExecutionMode.OUT_OF_PROCESS

/**
 * Abstract base class for building custom tasks for preparing GEMs.
 *
 * @author Schalk W. Cronjé
 * @author R Tyler Croy
 * @author Christian Meier
 */
@CompileStatic
class AbstractGemPrepareTask extends GrolifantDefaultTask implements GemPrepareTask {

    public static final String JRUBY_COMPLETE_NAME = GemUtils.JRUBY_COMPLETE_NAME

    @Override
    void setExecutionMode(ExecutionMode executionMode) {
        if (executionMode == ExecutionMode.CLASSPATH) {
            throw new UnsupportedConfigurationException('CLASSPATH is not a supported execution mode for JRuby')
        }
        this.executionMode = executionMode
    }

    @Override
    void setAwaitMode(Boolean aBoolean) {
        this.awaitMode = aBoolean
    }

    @Override
    void setGemOverwriteAction(GemOverwriteAction action) {
        extensions.getByType(GemPrepareTaskExtension).gemOverwriteAction = action
    }

    /**
     * The overwrite action to use for GEMs.
     *
     * @return Overwrite action.
     */
    @Internal
    GemOverwriteAction getGemOverwriteAction() {
        this.gemOverwriteActionProvider.get()
    }

    /**
     * Target directory for GEMs. Extracted GEMs will end up in {@code outputDir + "/gems"}
     *
     * @return Provider to a destination directory
     */
    @Override
    Provider<File> getOutputDir() {
        this.outputDir
    }

    /**
     * Sets the output directory
     *
     * @param f Output directory
     */
    @Override
    void setOutputDir(Object f) {
        extensions.getByType(GemPrepareTaskExtension).outputDir = f
    }

    @Override
    FileCollection getGemsAndJarsAsFileCollection() {
        this.gemsAndJars
    }

    /**
     * Sets the GEM configuration to be used with this task.
     *
     * @param configurationProvider File collection
     */
    @Override
    void setGemConfiguration(Configuration configurationProvider) {
        this.gemsAndJars = configurationProvider
        extensions.getByType(GemPrepareTaskExtension).gemConfiguration = configurationProvider
    }

    @TaskAction
    void exec() {
        if (!jrubyJar.present) {
            throw new MissingJRubyException(
                "Location of ${JRUBY_COMPLETE_NAME} is not set."
            )
        }

        final out = this.outputDir.get()
        final jars = jarsToProcess.get()

        gemsToDelete.get().each {
            if (it.directory) {
                it.deleteDir()
            } else {
                it.delete()
            }
        }

        out.mkdirs()
        writeJarsLock(jarsLockFile.get(), jars.coordinates)
        rewriteJarDependencies(jarRewriteDir.get(), jars, gemOverwriteAction)

        if (executionMode == JAVA_EXEC) {
            final result = execTools().javaexec { x -> execSpec.copyTo(x) }
            result.assertNormalExitValue()
        } else {
            WorkerPromise promise = execSpec.submitToWorkQueue(
                executionMode.workerIsolation(),
                workerExecutor,
                new StandardWorkerAppExecutorFactory(new JRubyWorkerExecutor()),
                new StandardWorkerAppParameterFactory()
            )
            if (awaitMode) {
                promise.await()
            }
        }
    }

    protected JRubyExecSpec foo() {
        this.execSpec
    }

    protected AbstractGemPrepareTask(WorkerExecutor we) {
        final gpte = extensions.create(
            GemPrepareTaskExtension.NAME,
            GemPrepareTaskExtension,
            project
        )

        this.jrubyJar = providerTools().property(File)

        this.execSpec = new JRubyExecSpec(this)
        this.workerExecutor = we
        this.outputDir = gpte.outputDir
        this.jarsToProcess = gpte.getJarsToProcess()
        this.gemsToDelete = gpte.gemsToDelete

        final gemPath = this.outputDir.map {
            it.absolutePath
        }

        this.jarsLockFile = outputDir.map { new File(it, JARS_LOCK) }
        this.jarRewriteDir = outputDir.map { new File(it, GemUtils.JARS_REWRITE_SUBDIR) }
        this.gemOverwriteActionProvider = gpte.gemOverwriteActionProvider

        final gemsToProcess = gpte.gemsToProcess
        execSpec.identity { rs ->
            rs.entrypoint {
                mainClass = GemUtils.JRUBY_MAINCLASS
                classpath(jrubyJar)
            }
            rs.jvm {
                environment JBUNDLE_SKIP: true,
                    JARS_SKIP: true,
                    GEM_HOME: gemPath,
                    GEM_PATH: gemPath
                systemProperties 'file.encoding': 'utf-8'

                if (OperatingSystem.current().windows) {
                    systemProperty('jdk.io.File.enableADS', true.toString())
                    // Workaround for FFI bug that is seen on some Windows environments
                    environment TMP: System.getenv('TMP'),
                        TEMP: System.getenv('TEMP')
                }
            }
            rs.script {
                setName(GEM_COMMAND)
                args 'install'
                addCommandLineArgumentProviders(
                    gemPath.map {
                        [
                            '--ignore-dependencies',
                            "--install-dir=${it}".toString(),
                            '--no-user-install',
                            '--wrappers',
                            '--no-document',
                            '--local'
                        ]
                    },
                    gemsToProcess.map { it.unpack*.absolutePath }
                )
            }
        }
    }

    /**
     * Location of {@code jruby-complete} JAR.
     *
     * The derived implementation needs to call this to set up the correct location.
     *
     * @param provider An object that will provide the location of jruby-complete.
     *   Must resolve to a {@code File} instance.
     */
    protected void setJrubyJarProvider(Object provider) {
        fsOperations().updateFileProperty(this.jrubyJar, provider)
    }

    private FileCollection gemsAndJars
    private final Provider<File> outputDir
    private final WorkerExecutor workerExecutor
    private final Provider<Set<File>> gemsToDelete
    private final Provider<File> jarsLockFile
    private final Provider<File> jarRewriteDir
    private final Provider<GemUtils.JarsToProcess> jarsToProcess
    private final Property<File> jrubyJar
    private ExecutionMode executionMode = OUT_OF_PROCESS
    private Boolean awaitMode = false
    private final JRubyExecSpec execSpec
    private final Provider<GemOverwriteAction> gemOverwriteActionProvider
}
