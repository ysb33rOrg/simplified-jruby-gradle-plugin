/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskProvider
import org.gradle.workers.WorkerExecutor
import org.ysb33r.gradle.jruby.api.core.JRubyEnvironmentInheritance
import org.ysb33r.gradle.jruby.api.core.JRubyExecSpec
import org.ysb33r.gradle.jruby.api.errors.MissingJRubyException
import org.ysb33r.gradle.jruby.api.gems.GemUtils
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmScriptExecTask

import static org.ysb33r.gradle.jruby.api.core.JRubyEnvironmentInheritance.ESSENTIAL_ONLY
import static org.ysb33r.gradle.jruby.api.gems.GemUtils.JARS_LOCK
import static org.ysb33r.gradle.jruby.api.gems.GemUtils.JRUBY_COMPLETE_NAME
import static org.ysb33r.gradle.jruby.internal.exec.JRubyExecUtils.prepareJRubyEnvironment

/**
 * Base class for setting up JRuby executions.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class AbstractJRubyExecTask extends AbstractJvmScriptExecTask<JRubyExecSpec> {

    /**
     * Set directory where GEMs are installed.
     *
     * @param gdp Provider to a task which will install GEMs.
     */
    void setGemDirProvider(TaskProvider<? extends AbstractGemPrepareTask> task) {
        fsOperations().updateFileProperty(
            this.gemDirProvider,
            task.map {
                it.outputDir.get()
            })
    }

    /**
     * Set directory where GEMs are installed.
     *
     * @param gdp Provider to a directory
     */
    void setGemDirProvider(Provider<File> gdp) {
        fsOperations().updateFileProperty(
            this.gemDirProvider,
            gdp
        )
    }

    /**
     * Allows for use script author to control the effect of the
     * system Ruby environment.
     *
     * @return How much of the JRuby system environment should be inherited.
     */
    @Input
    JRubyEnvironmentInheritance getInheritRubyEnv() {
        this.inheritEnv
    }

    /**
     * Allows for use script author to control the effect of the
     * system Ruby environment.
     *
     * @param mode How much of JRuby system environment should be inherited.
     */
    void setInheritRubyEnv(JRubyEnvironmentInheritance mode) {
        this.inheritEnv = mode
    }

    @Override
    void exec() {
        if (!jrubyJar.present) {
            throw new MissingJRubyException(
                "Location of ${JRUBY_COMPLETE_NAME} is not set."
            )
        }
        super.exec()
    }

    protected AbstractJRubyExecTask(WorkerExecutor we) {
        super(we)
        this.jrubyJar = project.objects.property(File)
        this.gemDirProvider = project.objects.property(File)
        this.jarsLockFile = gemDirProvider.map { new File(it, JARS_LOCK) }
        this.jarRewriteDir = gemDirProvider.map { new File(it, GemUtils.JARS_REWRITE_SUBDIR) }
        execSpec = new JRubyExecSpec(this)

        entrypoint {
            mainClass = GemUtils.JRUBY_MAINCLASS
            classpath(jrubyJar)
        }
        jvm {
            addEnvironmentProvider(project.provider { ->
                final keys = environment.keySet()
                stringTools().stringizeValuesDropNull(
                    prepareJRubyEnvironment(
                        keys,
                        inheritRubyEnv,
                        gemDirProvider,
                        jarRewriteDir,
                        jarsLockFile
                    )
                )
            })
            systemProperties 'file.encoding': 'utf-8'

            if (OperatingSystem.current().windows) {
                systemProperty('jdk.io.File.enableADS', true.toString())
                // Workaround for FFI bug that is seen on some Windows environments
                environment TMP: System.getenv('TMP'),
                    TEMP: System.getenv('TEMP')
            }
        }
    }

    /**
     * Location of {@code jruby-complete} JAR.
     *
     * The derived implementation needs to call this to set up the correct location.
     *
     * @param provider An object that will provide the location of jruby-complete.
     *   Must resolve to a {@code File} instance.
     */
    protected void setJrubyJarProvider(Object provider) {
        fsOperations().updateFileProperty(this.jrubyJar, provider)
    }

    private final Property<File> gemDirProvider
    private final Provider<File> jarsLockFile
    private final Provider<File> jarRewriteDir
    private final Property<File> jrubyJar
    private JRubyEnvironmentInheritance inheritEnv = ESSENTIAL_ONLY
}
