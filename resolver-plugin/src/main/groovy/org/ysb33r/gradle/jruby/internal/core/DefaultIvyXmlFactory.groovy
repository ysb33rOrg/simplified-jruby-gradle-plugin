/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.core

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.ysb33r.gradle.jruby.api.resolver.ApiException
import org.ysb33r.gradle.jruby.api.gems.GemNotFoundException
import org.ysb33r.gradle.jruby.api.resolver.IvyXmlFactory
import org.ysb33r.gradle.jruby.api.resolver.RubyGemQueryRestApi
import org.ysb33r.gradle.jruby.api.gems.GemInfo
import org.ysb33r.gradle.jruby.api.gems.GemVersion
import org.ysb33r.gradle.jruby.internal.gems.GemRepositoryConfiguration
import org.ysb33r.gradle.jruby.internal.gems.GemToIvy
import org.ysb33r.grolifant5.api.core.ExclusiveFileAccess

import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant

import static java.nio.file.Files.move
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING
import static org.ysb33r.gradle.jruby.api.gems.GemVersion.gemVersionFromGradleIvyRequirement
import static org.ysb33r.gradle.jruby.internal.core.IvyUtils.revisionsAsHtmlDirectoryListing

/**
 * Works with ivy.xml and relatd files.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
@Slf4j
class DefaultIvyXmlFactory implements IvyXmlFactory {
    DefaultIvyXmlFactory(
        File localCachePath,
        String group,
        URI serverUri,
        boolean refreshDependencies,
        GemRepositoryConfiguration grc
    ) {
        this.group = group
        this.refreshDependencies = refreshDependencies
        this.api = new DefaultRubyGemRestApi(serverUri)
        this.configuration = grc
        this.localCachePath = localCachePath
        this.gemToIvy = new GemToIvy(serverUri)
    }

    /**
     * Returns the path of disk where the ivy.xml file related to the specific Rubygem will be found.
     *
     * @param grp Maven-type group
     * @param name Rubygem name
     * @param version Rubygem version
     * @return Path on local file system
     * @throws GemNotFoundException
     */
    @Override
    Path ivyXmlPath(String grp, String name, String version) throws GemNotFoundException {
        if (inGroups(grp)) {
            String revision = getGemQueryRevisionFromIvy(name, version)
            Path ivyXml = ivyFile(name, revision)
            log.debug "Requested ${group}:${name}:${version} translated to GEM with version ${revision}"
            if (refreshDependencies || expired(ivyXml)) {
                try {
                    createIvyXml(ivyXml, name, revision)
                } catch (ApiException e) {
                    log.debug(e.message, e)
                    throw new GemNotFoundException()
                }
            }
            log.debug "Cached file is ${ivyXml.toAbsolutePath()}"
            log.debug "Cached file contains ${ivyXml.text}"
            ivyXml
        } else {
            throw new  GemNotFoundException()
        }
    }

    /**
     * Returns the path to the ivy.xml.sha1 file.
     *
     * @param grp Maven-type group
     * @param name Rubygem name
     * @param version Rubygem version
     * @return Path on local filesystem.
     * @throws GemNotFoundException
     */
    @Override
    Path ivyXmlSha1(String grp, String name, String version) throws GemNotFoundException {
        if (inGroups(grp)) {
            Path ivyXml = ivyXmlPath(grp, name, version)
            ivyXml.resolveSibling("${ivyXml.toFile().name}.sha1")
        } else {
            throw new GemNotFoundException()
        }
    }

    /**
     * Lists all versions as directories.
     *
     * @param grp Maven-type group
     * @param name Rubygem name
     * @return A directory listing of all versions
     * @throws GemNotFoundException
     */
    @Override
    String directoryListing(String grp, String name) throws GemNotFoundException {
        if (inGroups(grp)) {
            log.debug "Request to find all versions for ${grp}:${name}"
            List<String> versions = api.allVersions(name, configuration.prerelease)
            log.debug "Got versions ${versions.join(', ')}"
            revisionsAsHtmlDirectoryListing(versions)
        } else {
            throw new GemNotFoundException()
        }
    }

    @SuppressWarnings('UnusedMethodParameter')
    private Path ivyFile(String name, String revision) {
        new File(localCachePath, "${name}/${revision}/ivy.xml").toPath()
    }

    private String getGemQueryRevisionFromIvy(String gemName, String revisionPattern) {
        GemVersion version = gemVersionFromGradleIvyRequirement(revisionPattern)
        version.highOpenEnded ? api.latestVersion(gemName, configuration.prerelease) : version.high
    }

    @SuppressWarnings('BuilderMethodWithSideEffects')
    private void createIvyXml(Path ivyXml, String name, String revision) {
        ExclusiveFileAccess efa = new ExclusiveFileAccess(120000, 20)
        efa.access(ivyXml.toFile()) {
            GemInfo gemInfo = api.metadata(name, revision)
            ivyXml.parent.toFile().mkdirs()
            Path tmp = ivyXml.resolveSibling("${ivyXml.toFile().name}.tmp")
            tmp.withWriter { writer ->
                gemToIvy.writeTo(writer, gemInfo)
            }
            move(tmp, ivyXml, ATOMIC_MOVE, REPLACE_EXISTING)
            gemToIvy.writeSha1(ivyXml.toFile())
        }
    }
    private boolean inGroups(String grp) {
        grp == this.group
    }

    protected boolean expired(Path ivyXml) {
        System.currentTimeMillis()
        Path ivyXmlSha1 = ivyXml.resolveSibling("${ivyXml.toFile().name}.sha1")
        Files.notExists(ivyXml) || Files.notExists(ivyXmlSha1) ||
            (Files.getLastModifiedTime(ivyXml).toMillis() + EXPIRY_PERIOD_MILLIS < Instant.now().toEpochMilli())
    }

    private static final long EXPIRY_PERIOD_MILLIS =
        System.getProperty('org.ysb33r.gradle.jruby.cache-expiry-days', '15').toInteger() * 24 * 3600 * 1000

    private final boolean refreshDependencies
    private final String group
    private final RubyGemQueryRestApi api
    private final GemRepositoryConfiguration configuration
    private final File localCachePath
    private final GemToIvy gemToIvy
}
