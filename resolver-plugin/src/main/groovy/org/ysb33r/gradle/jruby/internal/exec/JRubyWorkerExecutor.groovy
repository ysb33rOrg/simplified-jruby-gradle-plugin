/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.exec

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.remote.worker.StandardWorkerAppExecutor

/**
 * Executes a JRuby script inside a worker.
 *
 * @author Schalk W> Cronjé
 */
@CompileStatic
class JRubyWorkerExecutor implements StandardWorkerAppExecutor {
    @Override
    Integer executeAndInterpret(Class<?> entrypoint, List<String> arguments) {
        executeAndInterpretImpl(entrypoint, arguments)
    }

//    private Integer executeAndInterpretImpl(Class<?> entrypoint, List<String> arguments) {
//        new org.jruby.Main(true)
//        final instance = entrypoint.newInstance([true] as Object[])
//        final result = instance.invokeMethod('run', arguments as String[])
//        if (result.exit) {
//            result.status
//        } else {
//            0
//        }
//    }
    @CompileDynamic
    private Integer executeAndInterpretImpl(Class<?> entrypoint, List<String> arguments) {
        final instance = entrypoint.newInstance([true] as Object[])
        final result = instance.invokeMethod('run', arguments as String[])
        if (result.exit) {
            result.status
        } else {
            0
        }
    }
}
