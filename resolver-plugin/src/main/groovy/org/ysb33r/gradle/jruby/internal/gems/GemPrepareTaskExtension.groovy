/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.gems

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.jruby.api.gems.GemOverwriteAction
import org.ysb33r.gradle.jruby.api.gems.GemUtils
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 * Used to ensure jar processing is configuration cache-safe.
 *
 * @author Schalk W. Cronjé
 *
 * @since 20.0
 */
@CompileStatic
class GemPrepareTaskExtension {

    public final static String NAME = 'jarProcessor'

    GemPrepareTaskExtension(Project project) {
        this.fsOperations = ProjectOperations.find(project).fsOperations
        this.outputDir = project.objects.property(File)
        this.jarsToProcessProvider = project.provider { ->
            GemUtils.findGemDependentJars(owner.depConfiguration)
        }

        this.gemOverwriteAction = project.objects.property(GemOverwriteAction)
        this.gemOverwriteAction.set(GemOverwriteAction.SKIP)

        this.gemsToProcessProvider = gemOverwriteAction.map { goa ->
                GemUtils.findGemsToProcess(
                    owner.depConfiguration,
                    outputDir.get(),
                    goa
                )
        }

        this.gemsToDeleteProvider = gemsToProcessProvider.map { it.delete }
    }

    Provider<File> getOutputDir() {
        this.outputDir
    }

    void setOutputDir(Object f) {
        fsOperations.updateFileProperty(this.outputDir, f)
    }

    void setGemConfiguration(Configuration configurationProvider) {
        this.depConfiguration = configurationProvider
    }

    Provider<GemUtils.GemsToProcess> getGemsToProcess() {
        this.gemsToProcessProvider
    }

    Provider<GemUtils.JarsToProcess> getJarsToProcess() {
        this.jarsToProcessProvider
    }

    Provider<Set<File>> getGemsToDelete() {
        this.gemsToDeleteProvider
    }

    void setGemOverwriteAction(GemOverwriteAction action) {
        this.gemOverwriteAction.set(action)
    }

    GemOverwriteAction getGemOverwriteAction() {
        this.gemOverwriteAction.get()
    }

    Provider<GemOverwriteAction> getGemOverwriteActionProvider() {
        this.gemOverwriteAction
    }

    private Configuration depConfiguration
    private final Property<GemOverwriteAction> gemOverwriteAction
    private final Property<File> outputDir
    private final Provider<GemUtils.GemsToProcess> gemsToProcessProvider
    private final Provider<GemUtils.JarsToProcess> jarsToProcessProvider
    private final Provider<Set<File>> gemsToDeleteProvider
    private final FileSystemOperations fsOperations
}
