/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.gems

import groovy.transform.CompileStatic

import javax.annotation.Nullable

/**
 * Utilities to deal with JRubyPrepare tasks
 *
 * @author Schalk W. Cronjé
 *
 */
@CompileStatic
class GemPrepareUtils {

    public static final String DEFAULT_CONFIGURATION = 'gems'
    public static final String DEFAULT_PREPARE_TASK = 'gemPrepare'

    /** JRubyPrepare task name by convention
     *
     * @param configurationName Name of a GEM configuration.
     * @return Associated task name.
     */
    static String taskName(String configurationName) {
        configurationName == DEFAULT_CONFIGURATION ? DEFAULT_PREPARE_TASK :
            "${DEFAULT_PREPARE_TASK}${configurationName.capitalize()}"
    }

    /** GEM working (unpack) relative path by convention.
     *
     * @param configurationName Name of a GEM configuration.
     * @return Associated relative path to project directory.
     */
    static String gemRelativePath(String configurationName) {
        configurationName == DEFAULT_CONFIGURATION ? '.gems' :
            ".gems-${configurationName}"
    }

    /**
     * Creates a name for a configuration.
     *
     * @param baseName Base name to use for ther configuration. Can be {@code null}.
     * @param taskName Name to use if there is an extension attached to a task.  Can be {@code null}.
     * @return
     */
    static String createJRubyConfigurationName(
        final String baseName,
        @Nullable final String taskName
    ) {
        final String task = taskName ? "_${taskName}" : ''
        "__@@${baseName}${task}@@__"
    }
}