/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.gems

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.util.logging.Slf4j
import org.gradle.api.Action
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.DependencyResolveDetails
import org.ysb33r.gradle.jruby.api.gems.GemResolverStrategy
import org.ysb33r.gradle.jruby.api.gems.GemVersion

import static org.ysb33r.gradle.jruby.api.gems.GemVersion.gemVersionFromGradleIvyRequirement

/**
 * Resolver to compute gem versions
 *
 * @author Schalk W. Cronjé
 * @author Christian Meier
 */
@CompileStatic
@Slf4j
class GemVersionResolver {

    static void addGemResolver(Project project, String configName, GemResolverStrategy gemgrp, GemVersionResolver resolver) {
        Action<DependencyResolveDetails> gemResolveRule = { DependencyResolveDetails drd ->
            if (gemgrp.useGemVersionResolver(configName) && gemgrp.useGemVersionResolver(drd.requested)) {
                resolver.resolve(drd)
            }
        }
        project.configurations.getByName(configName).resolutionStrategy.eachDependency(gemResolveRule)
    }

    static void addGemResolver(Configuration cfg, GemResolverStrategy gemGroups, GemVersionResolver versionResolver) {
        Action<DependencyResolveDetails> gemResolveRule = {
            String configName, GemResolverStrategy gemgrp, GemVersionResolver resolver, DependencyResolveDetails drd ->
                if (gemgrp.useGemVersionResolver(configName) && gemgrp.useGemVersionResolver(drd.requested)) {
                    resolver.resolve(drd)
                }
        }.curry(configurationName(cfg), gemGroups, versionResolver) as Action<DependencyResolveDetails>
        cfg.resolutionStrategy.eachDependency(gemResolveRule)
    }

    GemVersionResolver(GemResolverStrategy gemGroups, Configuration configuration) {
        this.gemGroups = gemGroups
        this.configuration = configuration
    }

    void resolve(DependencyResolveDetails details) {
        if (versions == null) {
            firstRun()
        }

        log.debug("${configuration}: gem ${details.requested.name} ${details.requested.version}")

        GemVersion version = versions[details.requested.name]

        if (version != null) {
            GemVersion next = version.intersect(details.requested.version)

            if (next.conflict()) {
                throw new GradleException("there is no overlap for ${versions[details.requested.name]} and ${details.requested.version}")
            }
            versions[details.requested.name] = next

            log.debug("${configuration}      collected ${version}")
            log.debug("${configuration}      resolved  ${next}")

            details.useVersion(next.toString())
            details.because('Selected by GEM Version Resolver')
        } else {
            GemVersion next = gemVersionFromGradleIvyRequirement(details.requested.version)
            versions[details.requested.name] = next
            log.debug("${configuration}      nothing collected")
            log.debug("${configuration}      resolved  ${next}")
        }
    }

    String toString() {
        return "GemVersionResolver${versions}"
    }

    protected void firstRun() {
        debugWithSeparator('collect version range info')
        Configuration config = configuration.copyRecursive()
        versions.clear()
        addGemResolver(config, gemGroups, this)
        config.resolvedConfiguration
        debugWithSeparator('apply version range info')
    }

    private void debugWithSeparator(final String text) {
        log.debug(
            "${configuration.name}\n${DBG_SEPARATOR}\n" +
                "                       ${text}\n${DBG_SEPARATOR}"
        )
    }

    private GemVersionResolver() {
    }

    // This is a workaround for compiling with Gradle 8,9, but having to be compatible to 7.3.3
    @CompileDynamic
    private static String configurationName(Configuration cfg) {
        cfg.name
    }

    @PackageScope
    static final GemVersionResolver NULL_RESOLVER = new GemVersionResolver()
    private static final String DBG_SEPARATOR = '                       ------------------------'
    private final Map versions = [:]
    private final Configuration configuration
    private final GemResolverStrategy gemGroups
}
