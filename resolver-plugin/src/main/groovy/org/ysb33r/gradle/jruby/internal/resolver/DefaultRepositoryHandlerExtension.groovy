/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.resolver

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.ArtifactRepository
import org.gradle.api.artifacts.repositories.IvyArtifactRepository
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.jruby.api.gems.GemResolverStrategy
import org.ysb33r.gradle.jruby.api.resolver.RepositoryHandlerExtension
import org.ysb33r.gradle.jruby.internal.gems.GemRepositoryConfiguration
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.StringTools

/**
 * Extension which can be added to {@code project.repositories}.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class DefaultRepositoryHandlerExtension implements RepositoryHandlerExtension {
    public static final String NAME = 'ruby'
    public static final String DEFAULT_GROUP_NAME = 'rubygems'

    /** Creates an extension and associate it with a project.
     *
     * @param project Gradle project.
     */
    DefaultRepositoryHandlerExtension(final Project project, Provider<IvyXmlProxyBuildService> ivyProxies) {
        this.extensions = project.extensions
        this.repositories = project.repositories
        this.ivyProxies = ivyProxies
        this.projectOperations = ProjectOperations.find(project)
        this.stringTools = this.projectOperations.stringTools
    }

    /** Create an artifact repository which will use https://rubygems.org and
     * associate group {@code rubygems} with it.
     *
     * @return Artifact repository.
     */
    @Override
    ArtifactRepository gems() {
        bindRepositoryToProxyServer(
            RUBYGEMS_URI,
            DEFAULT_GROUP_NAME,
            new GemRepositoryConfiguration()
        )
    }

    /** Create an artifact repository which will use https://rubygems.org and
     * associate group {@code rubygems} with it.
     *
     * @param cfg GEM repository configuration
     * @return Artifact repository.
     */
    ArtifactRepository gems(@DelegatesTo(GemRepositoryConfiguration) Closure cfg) {
        bindRepositoryToProxyServer(RUBYGEMS_URI, DEFAULT_GROUP_NAME, cfg)
    }

    /** Create an artifact repository which will use https://rubygems.org and
     * associate group {@code rubygems} with it.
     *
     * @param cfg GEM repository configuration
     * @return Artifact repository.
     */
    @Override
    ArtifactRepository gems(Action<GemRepositoryConfiguration> cfg) {
        bindRepositoryToProxyServer(RUBYGEMS_URI, DEFAULT_GROUP_NAME, cfg)
    }

    /** Create an artifact repository which will use specified URI and
     * associate group {@code rubygems} with it.
     *
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     * with {@code project.uri} can be provided.
     *
     * @return Artifact repository.
     */
    @Override
    ArtifactRepository gems(Object uri) {
        bindRepositoryToProxyServer(stringTools.urize(uri), DEFAULT_GROUP_NAME, new GemRepositoryConfiguration())
    }

    /** Create an artifact repository which will use specified URI and
     * associate group {@code rubygems} with it.
     *
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     * with {@code project.uri} can be provided.
     * @param cfg GEM repository configuration
     *
     * @return Artifact repository.
     */
    ArtifactRepository gems(Object uri, @DelegatesTo(GemRepositoryConfiguration) Closure cfg) {
        bindRepositoryToProxyServer(stringTools.urize(uri), DEFAULT_GROUP_NAME, cfg)
    }

    /** Create an artifact repository which will use specified URI and
     * associate group {@code rubygems} with it.
     *
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     * with {@code project.uri} can be provided.
     * @param cfg GEM repository configuration
     *
     * @return Artifact repository.
     */
    @Override
    ArtifactRepository gems(Object uri, Action<GemRepositoryConfiguration> cfg) {
        bindRepositoryToProxyServer(stringTools.urize(uri), DEFAULT_GROUP_NAME, cfg)
    }

    /** Create an artifact repository which will use specified URI and
     * associate a specified group with it.
     *
     * @param group Group to associate this server with.
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     * with {@code project.uri} can be provided.
     * @return Artifact repository.
     */
    @Override
    ArtifactRepository gems(String group, Object uri) {
        bindRepositoryToProxyServer(stringTools.urize(uri), group, new GemRepositoryConfiguration())
    }

    /** Create an artifact repository which will use specified URI and
     * associate a specified group with it.
     *
     * @param group Group to associate this server with.
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     * with {@code project.uri} can be provided.
     * @param cfg GEM repository configuration
     * @return Artifact repository.
     */
    ArtifactRepository gems(String group, Object uri, @DelegatesTo(GemRepositoryConfiguration) Closure cfg) {
        bindRepositoryToProxyServer(stringTools.urize(uri), group, cfg)
    }

    /** Create an artifact repository which will use specified URI and
     * associate a specified group with it.
     *
     * @param group Group to associate this server with.
     * @param uri URI of remote repository that serves up Rubygems. Any object convertible
     * with {@code project.uri} can be provided.
     * @param cfg GEM repository configuration
     * @return Artifact repository.
     */
    @Override
    ArtifactRepository gems(String group, Object uri, Action<GemRepositoryConfiguration> cfg) {
        bindRepositoryToProxyServer(stringTools.urize(uri), group, cfg)
    }

    /** Adds the Maven-GEMs proxy that is supported by the JRuby group.
     *
     * For supporting Gradle versions, this repository will only be consulted for artifacts that are in the
     * {@code rubygems} group.
     *
     * @return Maven repository
     */
    @Override
    MavenArtifactRepository mavengems() {
        bindToMavenRepository(MAVENGEMS_URI, DEFAULT_GROUP_NAME)
    }

    /** Adds a remote Maven-GEMs proxy.
     *
     * For supporting Gradle versions, this repository will only be consulted for artifacts that are in the
     * {@code rubygems} group.
     *
     * @param uri Remote Maven-GEMs proxy
     * @return Maven repository
     */
    @Override
    MavenArtifactRepository mavengems(Object uri) {
        bindToMavenRepository(stringTools.urize(uri), DEFAULT_GROUP_NAME)
    }

    /**  Adds a remote Maven-GEMs proxy anbd allocate a dedicated group for it.
     *
     * For supporting Gradle versions, this repository will only be consulted for artifacts that are in the
     * specified group.
     *
     * @param group Maven group name
     * @param uri Remote Maven-GEMs proxy
     * @return Maven repository
     */
    @Override
    MavenArtifactRepository mavengems(String group, Object uri) {
        bindToMavenRepository(stringTools.urize(uri), group)
    }

    private MavenArtifactRepository bindToMavenRepository(
        URI serverUri,
        String group
    ) {
        MavenArtifactRepository repo = repositories.maven(new Action<MavenArtifactRepository>() {
            @Override
            void execute(MavenArtifactRepository mvn) {
                mvn.url = serverUri
            }
        })
        restrictToGems(repo, group)
        repo
    }

    private ArtifactRepository bindRepositoryToProxyServer(
        URI serverUri,
        String group,
        GemRepositoryConfiguration cfg
    ) {
        URI bindAddress = ivyProxies.get().registerProxy(serverUri, group, cfg)
        extensions.getByType(GemResolverStrategy).addGemGroup(group)
        restrictToGems(createIvyRepo(serverUri, bindAddress), group)
    }

    private ArtifactRepository bindRepositoryToProxyServer(
        URI serverUri,
        String group,
        @DelegatesTo(GemRepositoryConfiguration) Closure cfg
    ) {
        GemRepositoryConfiguration grc = new GemRepositoryConfiguration()
        ClosureUtils.configureItem(grc, cfg)
        bindRepositoryToProxyServer(serverUri, group, grc)
    }

    private ArtifactRepository bindRepositoryToProxyServer(
        URI serverUri,
        String group,
        Action<GemRepositoryConfiguration> cfg
    ) {
        GemRepositoryConfiguration grc = new GemRepositoryConfiguration()
        cfg.execute(grc)
        bindRepositoryToProxyServer(serverUri, group, grc)
    }

    private IvyArtifactRepository createIvyRepo(URI server, URI bindAddress) {
        repositories.ivy { IvyArtifactRepository repo ->
            repo.tap {
                artifactPattern "${server}/downloads/[artifact]-[revision](-[classifier]).gem"
                ivyPattern "${bindAddress}/[organisation]/[module]/[revision]/ivy.xml"
                allowInsecureProtocol = true
            }
        }
    }

    private ArtifactRepository restrictToGems(ArtifactRepository repo, String group) {
        repo.content {
            it.includeGroup group
            repo
        }
    }

    private final ProjectOperations projectOperations
    private final StringTools stringTools
    private final Provider<IvyXmlProxyBuildService> ivyProxies
    private final ExtensionContainer extensions
    private final RepositoryHandler repositories
    private static final URI RUBYGEMS_URI = 'https://rubygems.org'.toURI()
    private static final URI MAVENGEMS_URI = 'https://mavengems.jruby.org'.toURI()
}
