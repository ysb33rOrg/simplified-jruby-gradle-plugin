/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.resolver

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import org.gradle.api.provider.Property
import org.gradle.api.services.BuildService
import org.gradle.api.services.BuildServiceParameters
import org.ysb33r.gradle.jruby.internal.core.DefaultIvyXmlFactory
import org.ysb33r.gradle.jruby.internal.gems.GemRepositoryConfiguration
import ratpack.server.RatpackServer

import java.security.MessageDigest
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

/**
 * Gradle build service that will proxy RubyGems entries to Ivy repository style.
 *
 * When instantiated, Gradle will automatically create a {@link BuildService#getParameters} method.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
@SuppressWarnings(['AbstractClassWithoutAbstractMethod', 'AbstractClassWithPublicConstructor'])
abstract class IvyXmlProxyBuildService implements
    BuildService<IvyXmlProxyBuildService.Params>,
    AutoCloseable {

    interface Params extends BuildServiceParameters {
        Property<Boolean> getRefreshDependencies();

        Property<File> getRootCacheDir();
    }

    IvyXmlProxyBuildService() {
        true
//        parameters.refreshDependencies
//        proxy = new RubyGemsProxyServer(CACHE,SERVERURI,GROUP,GRC)
//        this.registry = new IvyXmlGlobalProxyRegistry(parameters)
    }

    /**
     * Registers a proxy to a specific remote URI.
     *
     * @param remoteURI URI of Rubygems repository
     * @param group Local group associated with the repository. This is is an abitrary name which is purely used
     * to only resolve GEMs which are placed in this group against the provided Rubygems repository.
     * @param grc Additional configuration for the remote repository.
     * @return Bind URI to local Ivy repository
     */
    URI registerProxy(
        URI remoteURI,
        String group,
        GemRepositoryConfiguration grc
    ) {
        getOrCreateServer(
            remoteURI,
            group,
            new File(parameters.rootCacheDir.get(), uriHash(remoteURI)),
            grc
        )
    }

    /**
     * Closes all registered proxies.
     */
    @Override
    @Synchronized
    void close() {
        SERVER_MAP.forEach { URI ignored, RatpackServer server ->
            server.stop()
        }
        SERVER_MAP.clear()
    }

    private String uriHash(URI remoteURI) {
        MessageDigest.getInstance('SHA-1').digest(remoteURI.toString().bytes).encodeHex().toString()
    }

    private URI getOrCreateServer(
        URI uri,
        String group,
        File cacheDir,
        GemRepositoryConfiguration grc
    ) {
        RatpackServer server = SERVER_MAP.computeIfAbsent(uri) { URI it ->
            createProxyServer(it, group, cacheDir, grc)
        }
        "http://localhost:${server.bindPort}".toURI()
    }

    private RatpackServer createProxyServer(
        URI uri,
        String group,
        File cacheDir,
        GemRepositoryConfiguration grc
    ) {
        final DefaultIvyXmlFactory ivyXmlFactory = new DefaultIvyXmlFactory(
            cacheDir,
            group,
            uri,
            parameters.refreshDependencies.get(),
            grc
        )
        RatpackServerFactory.createAndStartServer(ivyXmlFactory, cacheDir)
    }

    static private final ConcurrentMap<URI, RatpackServer> SERVER_MAP = new ConcurrentHashMap<>()
}
