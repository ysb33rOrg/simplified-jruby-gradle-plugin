/**
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.internal.resolver;

import org.ysb33r.gradle.jruby.api.gems.GemNotFoundException;
import org.ysb33r.gradle.jruby.api.resolver.IvyXmlFactory;
import ratpack.server.RatpackServer;

import java.io.File;
import java.net.URI;
import java.nio.file.Path;

import static ratpack.server.RatpackServer.start;
import static ratpack.server.ServerConfig.embedded;

/**
 * Creates instances of Ratpack servers
 *
 * @author Schalk W. Cronje
 */
public class RatpackServerFactory {
    /*
    bindPort = server.
        URI getBindAddress() {
        "http://localhost:${bindPort}".toURI()
    }
     */

    static RatpackServer createAndStartServer(
            final IvyXmlFactory ivyXmlFactory,
            final File localCachePath
    ) {
        try {
            return start(server -> server
                    .serverConfig(
                            embedded()
                                    .publicAddress(new URI("http://localhost"))
                                    .port(0)
                                    .baseDir(localCachePath)
                    ).handlers(chain -> chain
                            .get(":group/:module/:revision/ivy.xml", ctx -> {
                                try {
                                    Path ivyXml = ivyXmlFactory.ivyXmlPath(
                                            ctx.getAllPathTokens().get("group"),
                                            ctx.getAllPathTokens().get("module"),
                                            ctx.getAllPathTokens().get("revision")
                                    );
                                    ctx.getResponse().contentType("text/xml").sendFile(ivyXml);
                                } catch (GemNotFoundException e) {
                                    ctx.clientError(404);
                                }
                            }).get(":group/:module/:revision/ivy.xml.sha1", ctx -> {
                                try {
                                    Path ivyXmlSha1 = ivyXmlFactory.ivyXmlSha1(
                                            ctx.getAllPathTokens().get("group"),
                                            ctx.getAllPathTokens().get("module"),
                                            ctx.getAllPathTokens().get("revision")
                                    );
                                    ctx.getResponse().contentType("text/plain").sendFile(ivyXmlSha1);
                                } catch (GemNotFoundException e) {
                                    ctx.clientError(404);
                                }
                            }).get(":group/:module", ctx -> {
                                try {
                                    String listing = ivyXmlFactory.directoryListing(
                                            ctx.getAllPathTokens().get("group"),
                                            ctx.getAllPathTokens().get("module")
                                    );
                                    ctx.getResponse().contentType("text/html").send(listing);
                                } catch (GemNotFoundException e) {
                                    ctx.clientError(404);
                                }
                            }).get(ctx -> ctx.clientError(403))
                    )
            );
        } catch (Exception e) {
            throw new RuntimeException("Could not start Ratpack", e);
        }
    }

}
