/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api

import org.gradle.workers.WorkerExecutor
import org.ysb33r.gradle.jruby.api.core.JRubyExtension
import org.ysb33r.gradle.jruby.api.gems.GemConfigurations
import org.ysb33r.gradle.jruby.api.gems.GemOverwriteAction
import org.ysb33r.gradle.jruby.api.gems.GemUtils
import org.ysb33r.gradle.jruby.api.plugins.JRubyResolverPlugin
import org.ysb33r.gradle.jruby.api.tasks.AbstractGemPrepareTask
import org.ysb33r.gradle.jruby.testfixtures.UnitTestSpecification

import javax.inject.Inject

class GemPrepareTaskSpec extends UnitTestSpecification {

    void 'Create a custom extension and prepare task'() {
        setup:
        String name = 'mygems'

        when:
        project.allprojects {
            // tag::setup-tasks[]
            project.pluginManager.apply(JRubyResolverPlugin)
            project.extensions.getByType(GemConfigurations).register(name, MyPrepareTask)
            // end::setup-tasks[]
        }

        then:
        project.configurations.getByName(name)

        when:
        final gemTask = (AbstractGemPrepareTask) project.tasks.getByName("gemPrepare${name.capitalize()}")

        then:
        gemTask.group == GemConfigurations.DEFAULT_TASK_GROUP
        gemTask.gemOverwriteAction == GemOverwriteAction.SKIP
        project.configurations.getByName("__@@jrubyx_gemPrepare${name.capitalize()}@@__")
    }

    static
    // tag::create-prepare-task[]
    // tag::create-prepare-task-with-extension[]
    class MyPrepareTask extends AbstractGemPrepareTask {
        @Inject
        MyPrepareTask(WorkerExecutor we) {
            super(we)
            // end::create-prepare-task[]
            this.jruby = extensions.create(JRubyExtension.NAME, JRubyExtension, this, 'jrubyx') // <1>
            setJrubyJarProvider(project.provider { -> // <2>
                GemUtils.findJRubyJar(jruby.jrubyConfiguration, jruby.jrubyVersion) // <3>
            })
            // tag::create-prepare-task[]
        }
        // end::create-prepare-task[]
        private final JRubyExtension jruby
        // tag::create-prepare-task[]
    }
    // end::create-prepare-task[]
    // end::create-prepare-task-with-extension[]
}