/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.core

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.jruby.api.gems.GemUtils
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import spock.lang.Specification

import static org.ysb33r.gradle.jruby.api.core.JRubyExecSpec.LOAD_JARS_LOCK_ON_STARTUP
import static org.ysb33r.gradle.jruby.api.tasks.AbstractGemPrepareTask.JRUBY_COMPLETE_NAME

class JRubyExecSpecSpec extends Specification {
    Project project = ProjectBuilder.builder().build()
    ProjectOperations po
    JRubyExecSpec jRubyExecSpec

    void setup() {
        project.pluginManager.apply(GrolifantServicePlugin)
        po = ProjectOperations.find(project)
        jRubyExecSpec = new JRubyExecSpec(ConfigCacheSafeOperations.from(project))
    }

    void 'When a script name is specified, -S is added to the command-line'() {
        when:
        jRubyExecSpec.entrypoint {
            mainClass = GemUtils.JRUBY_MAINCLASS
            classpath("/path/jar/${JRUBY_COMPLETE_NAME}")
        }
        jRubyExecSpec.script {
            name = 'gem'
            args 'install'
            addCommandLineArgumentProviders(
                project.provider { ->
                    [
                        '--ignore-dependencies',
                        '--install-dir=/foo/bar',
                        '--no-user-install',
                        '--wrappers',
                        '--no-document',
                        '--local'
                    ]
                },
                project.provider { -> ['/my/packaged.gem'] }
            )
        }

        final execSpec = po.jvmTools.javaExecSpec()
        jRubyExecSpec.copyTo(execSpec)

        then:
        verifyAll {
            execSpec.commandLine[0].endsWith('java') || execSpec.commandLine[0].endsWith('java.exe')
            execSpec.commandLine[1] == LOAD_JARS_LOCK_ON_STARTUP
            execSpec.commandLine[2] == '-S'
            execSpec.commandLine[3] == 'gem'
            execSpec.commandLine[4] == 'install'
            execSpec.commandLine[5] == '--ignore-dependencies'
            execSpec.commandLine[11] == '/my/packaged.gem'
        }
    }
}