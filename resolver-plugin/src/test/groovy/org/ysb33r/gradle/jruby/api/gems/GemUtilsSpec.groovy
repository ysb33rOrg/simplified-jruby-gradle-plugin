/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.gems

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DuplicateFileCopyingException
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.ProjectOperations
import spock.lang.Specification
import spock.lang.Unroll

import static org.ysb33r.gradle.jruby.api.gems.GemOverwriteAction.FAIL
import static org.ysb33r.gradle.jruby.api.gems.GemOverwriteAction.OVERWRITE
import static org.ysb33r.gradle.jruby.api.gems.GemOverwriteAction.SKIP

class GemUtilsSpec extends Specification {

    Project project
    ProjectOperations po
    File src
    File dest
    File fakeGem

    void setup() {
        project = ProjectBuilder.builder().build()
        po = ProjectOperations.maybeCreateExtension(project)
        src = project.file('src')
        dest = project.file('dest')
        fakeGem = new File(src, 'gems/mygem-1.0')

        src.mkdirs()
        dest.mkdirs()

        /* Creating a folder structure that should look like
         * src
         * src/cache
         * src/cache/cache.txt
         * src/gems
         * src/gems/mygem-1.0/fake.txt
         * src/gems/mygem-1.0/test
         * src/gems/mygem-1.0/test/test.txt
         */
        new File(fakeGem, 'test').mkdirs()
        new File(fakeGem, 'fake.txt').text = 'fake.content'
        new File(fakeGem, 'test/test.txt').text = 'test.content'

        new File(src, 'cache').mkdirs()
        new File(src, 'cache/cache.txt').text = 'cache.content'
    }

    void "Attach a fake gem folder to a copy command"() {
        when:
        po.fsOperations.copy({ CopySpec it ->
            it.into dest
            it.with GemUtils.gemCopySpec(po, src)
        } as Action<CopySpec>)

        then:
        new File(dest, 'gems/mygem-1.0/fake.txt').exists()
        !new File(dest, 'gems/mygem-1.0/test/test.txt').exists()
        !new File(dest, 'cache').exists()
    }

    void "Attach a fake gem folder to a copy command with fullGem=false"() {
        when:
        po.fsOperations.copy { CopySpec it ->
            it.into dest
            it.with GemUtils.gemCopySpec(po, src, fullGem: false)
        }

        then:
        new File(dest, 'gems/mygem-1.0/fake.txt').exists()
        !new File(dest, 'gems/mygem-1.0/test/test.txt').exists()
        !new File(dest, 'cache').exists()
    }

    void "Attach a fake gem folder to a copy command with subfolder"() {
        when:
        po.fsOperations.copy { CopySpec it ->
            it.into dest
            it.with GemUtils.gemCopySpec(po, src, subfolder: 'foo')
        }

        then:
        !new File(dest, 'gems/mygem-1.0/fake.txt').exists()
        !new File(dest, 'gems/mygem-1.0/test/test.txt').exists()
        !new File(dest, 'cache').exists()
        new File(dest, 'foo/gems/mygem-1.0/fake.txt').exists()
        !new File(dest, 'foo/gems/mygem-1.0/test/test.txt').exists()
        !new File(dest, 'foo/cache').exists()
    }

    void "Attach a fake gem folder to a copy command with fullGem=true"() {
        when:
        po.fsOperations.copy { CopySpec it ->
            it.into dest
            it.with GemUtils.gemCopySpec(po, src, fullGem: true)
        }

        then: "Expecting test and cache folders to be copied"
        new File(dest, 'gems/mygem-1.0/fake.txt').exists()
        new File(dest, 'gems/mygem-1.0/test/test.txt').exists()
        new File(dest, 'cache/cache.txt').exists()
    }

    void "Attach a fake gem folder to a copy command with subfolder and fullGem=true"() {
        when:
        po.fsOperations.copy { CopySpec it ->
            it.into dest
            it.with GemUtils.gemCopySpec(po, src, subfolder: 'foo', fullGem: true)
        }

        then:
        !new File(dest, 'gems/mygem-1.0/fake.txt').exists()
        !new File(dest, 'gems/mygem-1.0/test/test.txt').exists()
        !new File(dest, 'cache').exists()
        new File(dest, 'foo/gems/mygem-1.0/fake.txt').exists()
        new File(dest, 'foo/gems/mygem-1.0/test/test.txt').exists()
        new File(dest, 'foo/cache').exists()
    }

    void "write Jars.lock"() {
        when:
        File jarsLock = new File(dest, "jars.lock")
        jarsLock.delete()
        GemUtils.writeJarsLock(jarsLock, ['something'])

        then:
        jarsLock.text =~ /something/
    }

    void "skip write Jars.lock"() {
        when:
        File jarsLock = new File(dest, "jars.lock")
        jarsLock << 'something' << System.getProperty("line.separator")
        jarsLock.setLastModified(0)
        GemUtils.writeJarsLock(jarsLock, ['something'])

        then:
        jarsLock.text =~ /something/
        jarsLock.lastModified() == 0
    }

    void "overwrite write Jars.lock"() {
        when:
        File jarsLock = new File(dest, "jars.lock")
        jarsLock << ''
        GemUtils.writeJarsLock(jarsLock, ['something'])

        then:
        new File(dest, "jars.lock").text =~ /something/
    }

    @Unroll
    void "rewrite jar dependency when overwrite mode is #goa"() {
        setup:
        final content = 'something'
        File jars = new File(dest, 'jars')
        File jar = new File(dest, "${goa}.jar")
        jar << content
        final jtp = new GemUtils.JarsToProcess(
            [jar],
            ['FAIL.jar': 'fail.jar', 'SKIP.jar': 'skip.jar', 'OVERWRITE.jar': 'overwrite.jar']
        )

        when:
        GemUtils.rewriteJarDependencies(jars, jtp, goa)

        then:
        new File(jars, "${goa.toString().toLowerCase(Locale.US)}.jar").length() == content.length()

        where:
        goa << GemOverwriteAction.values()
    }

    void "skip rewrite jars dependency"() {
        setup:
        File jars = new File(dest, 'jars')
        File jar1 = new File(dest, "jar1.jar")
        File jar2 = new File(dest, "jar2.jar")
        File target1 = new File(jars, "jar1.jar")
        File target2 = new File(jars, "jar2.jar")
        jar1 << 'something'
        jar2 << 'something'
        jars.mkdir()
        target1 << ''
        target2.delete()
        final jtp = new GemUtils.JarsToProcess(
            [jar1, jar2],
            ['jar1.jar': 'jar1.jar', 'jar2.jar': 'jar2.jar']
        )

        when:
        GemUtils.rewriteJarDependencies(
            jars,
            jtp,
            SKIP
        )

        then:
        target1.length() == 0
        target2.length() == 9
    }

    void "overwrite rewrite jars dependency"() {
        setup:
        File jars = new File(dest, 'jars')
        File jar1 = new File(dest, "jar1.jar")
        File jar2 = new File(dest, "jar2.jar")
        File target1 = new File(jars, "jar1.jar")
        File target2 = new File(jars, "jar2.jar")
        jar1 << 'something'
        jar2 << 'something'
        jars.mkdir()
        target1 << ''
        target2.delete()
        final jtp = new GemUtils.JarsToProcess(
            [jar1, jar2],
            ['jar1.jar': 'jar1.jar', 'jar2.jar': 'jar2.jar']
        )

        when:
        GemUtils.rewriteJarDependencies(
            jars,
            jtp,
            OVERWRITE
        )

        then:
        target1.length() == 9
        target2.length() == 9
    }

    void "fail rewrite jars dependency"() {
        setup:
        File jars = new File(dest, 'jars')
        File jar = new File(dest, "jar.jar")
        File target = new File(jars, "jar.jar")
        jar << 'something'
        jars.mkdir()
        target << ''
        final jtp = new GemUtils.JarsToProcess(
            [jar],
            ['jar.jar': 'jar.jar']
        )
        when:
        GemUtils.rewriteJarDependencies(jars, jtp, FAIL)

        then:
        thrown(DuplicateFileCopyingException)
    }

    void "gemFullNameFromFile() should prune .gem"() {
        given:
        String filename = "rake-10.3.2.gem"
        String gem_name = "rake-10.3.2"

        expect:
        gem_name == GemUtils.gemFullNameFromFile(filename)
    }
}
