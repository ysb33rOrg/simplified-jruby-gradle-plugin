/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.api.resolver

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.jruby.api.plugins.JRubyResolverPlugin
import spock.lang.Specification

class RepositoryHandlerExtensionSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    void 'Add Maven repository'() {
        when:
        project.allprojects {
            apply plugin: JRubyResolverPlugin

            // tag::using-mavengems[]
            repositories {
                ruby {
                    mavengems() // <1>
                    mavengems('https://goo1') // <2>
                    mavengems('goo2', 'https://goo2') // <3>
                }
            }
            // end::using-mavengems[]

            // tag::using-gems[]
            repositories {
                ruby.gems('https://other-url') // <1>
                ruby.gems('otherGroup', 'https://other-url') // <2>
            }
            // end::using-gems[]
        }

        then:
        project.repositories.size() == 5
    }
}