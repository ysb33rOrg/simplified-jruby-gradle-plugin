/*
 * Copyright (c) 2024, as per respective authors listed in this file
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.ysb33r.gradle.jruby.testfixtures

import org.gradle.testkit.runner.GradleRunner
import spock.lang.Specification

class IntegrationTestSpecification extends Specification {
    File tempDir
    File projectDir
    File buildFile
    File testKitDir

    void setup() {
        tempDir = File.createTempDir('jruby-simple', '$$$')
        projectDir = new File(tempDir, 'test-project')
        buildFile = new File(projectDir, 'build.gradle')
        testKitDir = new File(tempDir, '.testKit')
        testKitDir.deleteDir()
        projectDir.deleteDir()
        testKitDir.mkdirs()
        projectDir.mkdirs()
    }

    void cleanup() {
        if (tempDir) {
            tempDir.deleteDir()
        }
    }

    GradleRunner getGradleRunner(List<String> args) {
        GradleRunner.create()
            .withDebug(true)
            .withProjectDir(projectDir)
            .withPluginClasspath()
            .withArguments(args + ['-i'])
            .withTestKitDir(testKitDir)
            .forwardOutput()
    }

    GradleRunner getGradleRunnerConfigCache(List<String> args) {
        getGradleRunner(args + ['--configuration-cache', '--configuration-cache-problems=fail'])
            .withGradleVersion('8.9')
    }

}